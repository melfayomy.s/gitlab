import initCodeReviewAnalyticsApp from 'ee/analytics/code_review_analytics';

document.addEventListener('DOMContentLoaded', () => {
  initCodeReviewAnalyticsApp();
});
